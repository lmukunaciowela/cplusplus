struct etudiant
{
	char *nom;
	char *prenom;
	int age;
	etudiant * ami;
};
etudiant* extend(etudiant p[], const int size, const int n);
void libereTab(etudiant *p, const int size);
etudiant * creerEtudiant(char* nom, char* pnom,int y);
int ajouterAmi(etudiant* etu,etudiant* new_friend);
bool estAmiDe(etudiant* e1, etudiant* e2, int& indice);
int supprimerAmi(etudiant * etud, etudiant * old_friend);