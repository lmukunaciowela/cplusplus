#include<stdlib.h>
#include"resocial.h"
#include<iostream>
using namespace std;
etudiant* extend(etudiant p[], const int size, const int n)
{
	int i = 0;

	etudiant* tab = new etudiant[size+n]; //(int*)malloc(sizeof(int)*(size + n));
	for (int a = 0; a < size; a++)
	{
		tab[a] = p[a];
	}
	return tab;
}
void libereTab(etudiant p[], const int size)
{
	delete p;
}
etudiant * creerEtudiant(char* nom, char* pnom, int y)
{
	etudiant *etu=new etudiant;
	etu->nom=nom;
	etu->prenom = pnom;
	etu->age = y;
	etu->ami = { NULL };
	return etu;
}
int ajouterAmi(etudiant* etu, etudiant* new_friend)
{
	if (etu->ami == new_friend)
	{
		return -1;
	}
	else
	{
		etu->ami = new_friend;
		return 0;
	}
}
bool estAmiDe(etudiant* e1, etudiant* e2, int& indice)
{
	while (e1->ami!=NULL || e2->ami!=NULL)
	{
		if (e1->ami == e2->ami)
		{
			return 0;
		}
		else
		{
			return 1;
		}
		if (e2->ami == e1->ami)
		{
			return 2;
		}
		if (e1->ami==e2->ami && e2->ami==e1->ami )
		{
			return 3;
		}
	}
}
int supprimerAmi(etudiant * etud, etudiant * old_friend)
{
	while (etud->ami != NULL)
	{
		if (etud->ami == old_friend)
		{
			delete old_friend;
			return 0;
		}
		else
		{
			return -1;
		}
	}
}